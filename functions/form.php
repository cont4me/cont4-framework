<?php

function form_open($action, $method = null, $enctype = null, $class = null, $id = null)
{
    if (!$method) $method = 'POST';
    if (!$class) $class = 'form account-form';
    if ($enctype) $enctype = 'enctype="multipart/form-data"';

    echo '<form class="' . $class . '" method="' . $method . '" id="' . $id . '" action="' . BASE . $action . '" ' . $enctype . '>';

}

function form_close()
{

    echo '</form>';
}

function form_checkbox_sup($label, $name, $value)
{
    echo '<div class="form-group">
              <label class="text-semibold">' . $label . '</label>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="' . $name . '" value="' . $value . '">
                </label>
              </div>
        </div>';
}
function form_checkbox($label, $name, $value, $data = null)
{
    if($value == $data) {
        $checked = 'checked';
    } else {
        $checked = '';
    }


    echo '<label>
                <input type="checkbox" name="' . $name . '" id="' . $name . '" value="' . $value . '" '.$checked.' style="margin-top: 35px;">
                    ' . $label . '
          </label>';

}

function form_text_input($label, $name, $data_validator = null, $server = null, $class = null, $value = null)
{

    if (isset($_POST[$name])) {
        $value = $_POST[$name];
    } elseif (!empty($value)) {
        $value = $value;
    } else {
        $value = '';
    }

    if (isset($data_validator)) {
        $rules = 'data-validation="' . $data_validator . '" ';

        if (isset($server)) {
            $rules .= 'data-validation-url="' . BASE . $server . '" ';
        }
    } else {
        $rules = '';
    }

    echo '<div class="form-group">
            <label for="' . $name . '">' . $label . '</label>
            <input type="text" name="' . $name . '" class="form-control" id="' . $name . '" value="' . $value . '" ' . $rules . ' autocomplete="off">
          </div>';
}


function form_int_input($label, $name, $data_validator = null, $server = null, $class = null, $value = null)
{

    if (isset($_POST[$name])) {
        $value = $_POST[$name];
    } elseif (!empty($value)) {
        $value = $value;
    } else {
        $value = '';
    }

    if (isset($data_validator)) {
        $rules = 'data-validation="' . $data_validator . '" ';

        if (isset($server)) {
            $rules .= 'data-validation-url="' . BASE . $server . '" ';
        }
    } else {
        $rules = '';
    }

    echo '<div class="form-group">
            <label for="' . $name . '">' . $label . '</label>
            <input type="number" name="' . $name . '" class="form-control" id="' . $name . '" value="' . $value . '" ' . $rules . ' autocomplete="off">
          </div>';
}

function form_file_input($label, $name, $data_validator = null, $server = null, $class = null, $value = null)
{

    if (isset($_POST[$name])) {
        $value = $_POST[$name];
    } elseif (!empty($value)) {
        $value = $value;
    } else {
        $value = '';
    }

    if (isset($data_validator)) {
        $rules = 'data-validation="' . $data_validator . '" ';

        if (isset($server)) {
            $rules .= 'data-validation-url="' . BASE . $server . '" ';
        }
    } else {
        $rules = '';
    }

    echo '<div class="form-group">
            <label for="' . $name . '">' . $label . '</label>
            <input type="file" name="' . $name . '" class="form-control" id="' . $name . '" value="' . $value . '" ' . $rules . '>
          </div>';
}

function form_password_input($label, $name, $data_validator = null, $server = null, $class = null, $value = null)
{

    if (isset($_POST[$name])) {
        $value = $_POST[$name];
    } elseif (!empty($value)) {
        $value = $value;
    } else {
        $value = '';
    }

    if (isset($data_validator)) {
        $rules = 'data-validation="' . $data_validator . '" ';

        if (isset($server)) {
            $rules .= 'data-validation-url="' . BASE . $server . '" ';
        }
    } else {
        $rules = '';
    }

    echo '<div class="form-group">
            <label for="' . $name . '">' . $label . '</label>
            <input type="password" name="' . $name . '" class="form-control" id="' . $name . '" value="' . $value . '" ' . $rules . ' autocomplete="off">
          </div>';
}



function submit($name, $class = null, $mt = null)
{
    $style = '';
    if ($mt > 0) {
        $style = 'style="margin-top: ' . $mt . 'px;"';
    }
    echo '<div class="form-group">
        <input type="submit" class="' . $class . '" value="' . $name . '" ' . $style . '>
    </div>';
}

function hidden($name, $value)
{
    echo '<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '">';
}


function form_select2($label, $name, $data, $fielddata, $value = null)
{
    $options = '';
    foreach ($data as $d) {
        $selected = '';
        if (!empty($value)) {
            if ($d->id == $value) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
        }

        $options .= '<option value="' . $d->id . '" ' . $selected . '>' . $d->$fielddata . '</option>';
    }

    echo '<div class="form-group">
        <label for="' . $name . '">' . $label . '</label>

          <select id="' . $name . '" name="' . $name . '" class="form-control">
              ' . $options . '
          </select>
        </div>

        <script type="text/javascript">
            $(function () {
              $("#' . $name . '").select2();
              $(".select2").removeAttr("style");
            })
        </script>';
}

function form_select2_ajax_value($link, $label, $name, $value = null, $table = null, $field = null)
{
    if(isset($table)) {
        $db = new db();
        $d = $db->row("SELECT * FROM " . $table. " WHERE id = " . $value);
    }

    if($d) {
        $option = "<option value='".$value."'>".$d->$field."</option>";
    }
    echo '<div class="form-group">
        <label for="' . $name . '">' . $label . '</label>

          <select id="' . $name . '" name="' . $name . '" class="form-control">
            '.$option.'
          </select>
        </div>

        <script type="text/javascript">
            $(function () {
              $("#' . $name . '").select2({
              ajax: { 
               url: "'.BASE.$link.'",
               type: "post",
               dataType: \'json\',
               delay: 250,
               data: function (params) {
                return {
                  searchTerm: params.term // search term
                };
               },
               processResults: function (response) {
                 return {
                    results: response
                 };
               },
               cache: true
              }
             });
              $(".select2").removeAttr("style");
            })
        </script>';
}

function form_select2_ajax($link, $label, $name)
{

    echo '<div class="form-group">
        <label for="' . $name . '">' . $label . '</label>

          <select id="' . $name . '" name="' . $name . '" class="form-control">
          </select>
        </div>

        <script type="text/javascript">
            $(function () {
              $("#' . $name . '").select2({
              ajax: { 
               url: "'.BASE.$link.'",
               type: "post",
               dataType: \'json\',
               delay: 250,
               data: function (params) {
                return {
                  searchTerm: params.term // search term
                };
               },
               processResults: function (response) {
                 return {
                    results: response
                 };
               },
               cache: true
              }
             });
              $(".select2").removeAttr("style");
            })
        </script>';
}

function form_select2_ajaxs($link, $label, $name, $value, $table, $field, $n)
{   
    $db = new Db();

    $c = $db->row("SELECT * FROM " . $table ." WHERE ". $field . " = '".$value."'");

    if($c) {
        $nome = $c->$n;
    } else {
        $nome = 'N/A';
    }


    echo '<div class="form-group">
        <label for="' . $name . '">' . $label . '</label>
          <select id="' . $name . '" name="' . $name . '" class="form-control">
            <option value="'.$value.'">'.$nome.'</option>
          </select>
        </div>

        <script type="text/javascript">
            $(function () {
              $("#' . $name . '").select2({
              ajax: { 
               url: "'.BASE.$link.'",
               type: "post",
               dataType: \'json\',
               delay: 250,
               data: function (params) {
                return {
                  searchTerm: params.term // search term
                };
               },
               processResults: function (response) {
                 return {
                    results: response
                 };
               },
               cache: true
              }
             });
              $(".select2").removeAttr("style");
            })
        </script>';
}




function form_select2_blank($label, $name, $data, $fielddata, $value = null)
{
    $options = '<option value="" > - </option>';
    foreach ($data as $d) {
        $selected = '';
        if (!empty($value)) {
            if ($d->id == $value) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
        }

        $options .= '<option value="' . $d->id . '" ' . $selected . '>' . $d->$fielddata . '</option>';
    }

    echo '<div class="form-group">
        <label for="' . $name . '">' . $label . '</label>

          <select id="' . $name . '" name="' . $name . '" class="form-control">
              ' . $options . '
          </select>
        </div>

        <script type="text/javascript">
            $(function () {
              $("#' . $name . '").select2();
              $(".select2").removeAttr("style");
            })
        </script>';
}


function form_select2_multiple($label, $name, $data, $fielddata, $value = [])
{
    $options = '';
    foreach ($data as $d) {
        $selected = '';
        if (!empty($value)) {
            foreach ($value as $v) {
                if ($d->id == $v->categoria) {
                    $selected = 'selected';
                }
            }
        }

        $options .= '<option value="' . $d->id . '" ' . $selected . '>' . $d->$fielddata . '</option>';
    }

    echo '<div class="form-group">
        <label for="' . $name . '">' . $label . '</label>

          <select multiple="" id="' . $name . '" name="' . $name . '[]" class="form-control">
              ' . $options . '
          </select>
        </div>

        <script type="text/javascript">
            $(function () {
              $("#' . $name . '").select2();
              $(".select2").removeAttr("style");
            })
        </script>';
}

function form_select($label, $name, $data, $fielddata, $value = null)
{
    $options = '';
    foreach ($data as $d) {
        $selected = '';
        if (!empty($value)) {
            if ($d->id == $value) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
        }

        $options .= '<option value="' . $d->id . '" ' . $selected . '>' . $d->$fielddata . '</option>';
    }

    echo '<div class="form-group">
        <label for="' . $name . '">' . $label . '</label>

          <select id="' . $name . '" name="' . $name . '" class="form-control">
              ' . $options . '
          </select>
        </div>';
}

function form_select_blank($label, $name, $data, $fielddata, $value = null)
{
    $options = '<option value=""> - </option>';
    foreach ($data as $d) {
        $selected = '';
        if (!empty($value)) {
            if ($d->id == $value) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
        }

        $options .= '<option value="' . $d->id . '" ' . $selected . '>' . $d->$fielddata . '</option>';
    }

    echo '<div class="form-group">
        <label for="' . $name . '">' . $label . '</label>

          <select id="' . $name . '" name="' . $name . '" class="form-control">
              ' . $options . '
          </select>
        </div>';
}

function form_select2_data($label, $name, $data, $value = null)
{

    $options = '';
    foreach ($data as $d) {
        $selected = '';
        if (!empty($value)) {
            if ($d['value'] == $value) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
        }

        $options .= '<option value="' . $d['value'] . '" ' . $selected . '>' . $d['nome'] . '</option>';
    }

    echo '<div class="form-group">
        <label for="' . $name . '">' . $label . '</label>

          <select id="' . $name . '" name="' . $name . '" class="form-control">
              ' . $options . '
            </optgroup>
          </select>
        </div>

        <script type="text/javascript">
            $(function () {
              $("#' . $name . '").select2();
              $(".select2").removeAttr("style");
            })
        </script>';
}

function form_select2_data_blank($label, $name, $data, $value = null)
{

    $options = '<option value=""> - </option>';
    foreach ($data as $d) {
        $selected = '';
        if (!empty($value)) {
            if ($d['value'] == $value) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
        }

        $options .= '<option value="' . $d['value'] . '" ' . $selected . '>' . $d['nome'] . '</option>';
    }

    echo '<div class="form-group">
        <label for="' . $name . '">' . $label . '</label>

          <select id="' . $name . '" name="' . $name . '" class="form-control">
              ' . $options . '
            </optgroup>
          </select>
        </div>

        <script type="text/javascript">
            $(function () {
              $("#' . $name . '").select2();
              $(".select2").removeAttr("style");
            })
        </script>';
}

function form_textarea($label, $name, $data_validator = null, $server = null, $class = null, $value = null)
{

    if (isset($_POST[$name])) {
        $value = $_POST[$name];
    } elseif (!empty($value)) {
        $value = $value;
    } else {
        $value = '';
    }

    if (isset($data_validator)) {
        $rules = 'data-validation="' . $data_validator . '" ';

        if (isset($server)) {
            $rules .= 'data-validation-url="' . BASE . $server . '" ';
        }
    } else {
        $rules = '';
    }

    echo '<div class="form-group">
            <label for="' . $name . '">' . $label . '</label>
            <textarea style="height:125px" name="' . $name . '" class="form-control"  id="' . $name . '"' . $rules . '>' . $value . '</textarea>
          </div>';
}
