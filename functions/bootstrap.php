<?php

function ptable($title)
{
    echo '<div class="portlet portlet-boxed portlet-table">
    <div class="portlet-header">
    <h4 class="portlet-title">
    <u>' . $title . '</u>
    </h4>
    </div>
    <div class="portlet-body">';

}

function opanel($title)
{
    echo '<div class="portlet portlet-boxed">
    <div class="portlet-header">
    <h4 class="portlet-title">
    ' . $title . '
    </h4>
    </div> 
    <div class="portlet-body" style="padding: 15px;">';
}

function cpanel()
{
    echo '</div> 
    </div>';
}

function div($class = null, $id = null)
{
    echo '<div class="' . $class . '" id="' . $id . '">';
}

function row()
{
    echo '<div class="row">';
}

function col($n)
{
    echo '<div class="col-md-' . $n . '">';
}

function cdiv()
{
    echo '</div>';
}

function img($src, $width = null, $class = null)
{
    $w = (isset($width)) ? 'width="' . $width . '"' : '';
    $c = (isset($class)) ? 'class="' . $class . '"' : '';
    echo '<img src="' . PUBLIC_URL . $src . '" '. $w . $c . '>';
}


function p($p, $class = null, $id = null, $style = null)
{
    echo "<p class='".$class."' style='".$style."' id='".$id."'>" . $p . "</p>";
}

function br()
{
    echo "<br><br>";
}

function hr()
{
    echo "<hr>";
}


function a($name, $link, $class = '')
{
    echo '<a href="' . BASE . $link . '" class="' . $class . '" id="'.$name.'" style="margin-right: 5px;">' . $name . '</a>';
}


function datatable($id, $header = [], $fields = [], $data = [], $buttons = true)
{
    $th = '';
    foreach ($header as $h) {
        $th .= "<th>" . $h . "</th>";
    }

    if ($buttons) {
        $th .= "<th> - </th>";
    }

    $tr = '';

    foreach ($data as $d) {
        $tr .= "[";
        foreach ($fields as $f) {
            if ($f == 'regra') {
                $tr .= '"' . retorna_regra($d->$f) . '",';
            } 
            elseif ($f == 'revisado') {
                $tr .= '"' . retorna_revisado($d->$f) . '",';
            } elseif ($f == 'aprovado') {
                $tr .= '"' . retorna_aprovado($d->$f) . '",';
            } elseif ($f == 'autorizado') {
                $tr .= '"' . retorna_autorizado($d->$f) . '",';
            } 
            elseif ($f == 'valor') {
                $tr .= '" R$ ' . moeda_real($d->$f) . '",';
            } 
            elseif ($f == 'valor_total') {
                $tr .= '" R$ ' . moeda_real($d->$f) . '",';
            } 
            elseif ($f == 'valor_pago') {
                $tr .= '" R$ ' . moeda_real($d->$f) . '",';
            } 
            elseif ($f == 'data_vencimento') {
                $tr .= '"' . data_br($d->$f) . '",';
            } 
            elseif ($f == 'data') {
                $tr .= '"' . data_br($d->$f) . '",';
            } 
            elseif ($f == 'origem') {
                $tr .= '"' . nome_select('tasks', 'origens', $d->$f) . '",';
            } 
            elseif ($f == 'classificacao') {
                $tr .= '"' . nome_select('tasks', 'classificacoes', $d->$f) . '",';
            } 
            
            elseif ($f == 'tipo' && $d->$f > 0) {
                $tr .= '"' . nome_select('tasks', 'tipos', $d->$f) . '",';
            } 
            
            elseif ($f == 'tipo' && ($d->$f != 'Interno' && $d->$f != 'Externo')) {
                $tr .= '"' . retorna_tipo($d->$f, (!empty($d->recorrente)) ? $d->recorrente : '') . '",';
            } 
            elseif ($f == 'data_criacao') {
                $tr .= '"' . data_br_completa($d->$f) . '",';
            } elseif ($f == 'data_final') {
                $tr .= '"' . data_br($d->$f) . '",';
            } 
            elseif ($f == 'data_lancamento') {
                $tr .= '"' . data_br($d->$f) . '",';
            } 
            elseif ($f == 'data_cadastro') {
                $tr .= '"' . data_br($d->$f) . '",';
            } 
            elseif ($f == 'data_pagamento') {
                $tr .= '"' . data_br($d->$f) . '",';
            }
            elseif ($f == 'status') {
                $tr .= '"' . retorna_tarefa_status($d->$f) . '",';
            } else {
                $tr .= '"' . $d->$f . '",';
            }

        }

        if ($buttons) {
            $tr .= '"';
            if (!empty($buttons['detalhes'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['detalhes'] . "=" . $d->id . "' title='Visualizar'><i class='fa fa-search fa-lg'></i> </a>";
            }
            if (!empty($buttons['download'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' target='_blank' href='" . $d->link . "' title='Baixar'><i class='fa fa-download fa-lg'></i> </a>";
            }
            if (!empty($buttons['editar'])) {
                if(isset($d->status) && $d->status == 'Concluído') {
                    $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['editar'] . "=" . $d->id . "' title='Visualizar'><i class='fa fa-search fa-lg'></i> </a>";
                } else {
                    $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['editar'] . "=" . $d->id . "' title='Editar'><i class='fa fa-edit fa-lg'></i> </a>";
                }
            }

            if (!empty($buttons['treinamentos'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['treinamentos'] . "=" . $d->id . "' title='Treinamentos'><i class='fa fa-certificate fa-lg'></i> </a>";
            }


            if (!empty($buttons['pdf'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['pdf'] . "=" . $d->id . "' title='Gerar PDF'><i class='fa fa-file fa-lg'></i> </a>";
            }

            if (!empty($buttons['email_modal'])) {
                $db = new Db;
                $email = $db->row("SELECT * FROM crm_empresas WHERE id = " . $d->cliente_fornecedor);
                $tr .= "<a style='text-decoration: none;margin-right: 2px;' data-email='" . $email->email . "' data-id='" . $d->id . "' id='mail' href='#email' data-toggle='modal' title='Enviar por e-mail'><i class='fa fa-envelope fa-lg'></i></a>";
            }

            if (!empty($buttons['deletar'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['deletar'] . "=" . $d->id . "' title='Excluir'><i class='fa fa-trash fa-lg'></i> </a>";
            }

            if (!empty($buttons['confirmar_pagamento'])) {
                if ($d->status == "aberto") {
                    $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['confirmar_pagamento'] . "=" . $d->id . "' title='Confirmar'><i class='fa fa-check fa-lg'></i> </a>";
                }
            }

            if (!empty($buttons['senha'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['senha'] . "=" . $d->id . "' title='Alterar senha'><i class='fa fa-key fa-lg'></i> </a>";
            }

            if (!empty($buttons['aulas'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['aulas'] . "=" . $d->id . "' title='Aulas'><i class='fa fa-graduation-cap fa-lg'></i> </a>";
            }
            if (!empty($buttons['alunos'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['alunos'] . "=" . $d->id . "' title='Alunos'><i class='fa fa-users fa-lg'></i> </a>";
            }

            if (!empty($buttons['cancelar'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['cancelar'] . "=" . $d->id . "' title='Excluir'><i class='fa fa-ban fa-lg'></i> </a>";
            }


            if (!empty($buttons['inativar'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['inativar'] . "=" . $d->id . "' title='Inativar documento'><i class='fa fa-ban fa-lg'></i> </a>";
            }

          

            if (!empty($buttons['concluir'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['concluir'] . "=" . $d->id . "' title='Concluír'><i class='fa fa-check fa-lg'></i> </a>";
            }

            $tr .= '",';
        }
        $tr .= "],";
    }

    echo '<div class="table-responsive"><table id="' . $id . '" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>' . $th . '</tr>
    </thead>
    </table></div>';

    echo '<script type="text/javascript">
    var data = [
    ' . $tr . '
    ]

    $("#' . $id . '").DataTable( {
        data: data
        } );
        </script>';
}


function table($id, $header = [], $fields = [], $data = [], $buttons = true)
{
    $th = '';
    foreach ($header as $h) {
        $th .= "<th>" . $h . "</th>";
    }

    if ($buttons) {
        $th .= "<th> - </th>";
    }

    $tr = '';

    foreach ($data as $d) {
        $tr .= "<tr>";
        foreach ($fields as $f) {
            if ($f == 'regra') {
                $tr .= '' . retorna_regra($d->$f) . '</td>';
            } 
            
            elseif($f == 'saldo')
            {
                $db = new Db();

                $sql = ' ';
                $sql .= " AND status = 'pago'"; 
                if(get('fconta')) 
                {
                    $sql .= " AND banco = '".get('fconta')."'";
                }

                $sql .= " ORDER BY data_vencimento"; 
                
                $valor_total_receitas = $db->table("SELECT sum(valor_pago) as total FROM movimentos WHERE tipo='receita' AND  data_vencimento <= '".$d->data_vencimento."'"  . $sql);

                foreach($valor_total_receitas as $vt) 
                {
                    $saldo = $vt->total;
                }

                $valor_total_despesas = $db->table("SELECT sum(valor_pago) as total FROM movimentos WHERE tipo='despesa' AND  data_vencimento <= '".$d->data_vencimento."'"  . $sql);

                foreach($valor_total_despesas as $vd) 
                {
                    $saldod = $vd->total;
                }


                $tr .= '<td> R$ ' . moeda_real($saldo - $saldod) . '</td>';

            }

            
            elseif ($f == 'nome_produto_ordem') {
                $db = new db();
                $orden = $db->row("SELECT * FROM ordens WHERE id = ". $d->orden);
                $tr .= '<td>';
                $tr .= $d->nome . '<br>';
                if($orden->tipo != 'pedido') {
                    $tr.= '<a class="text-success" href="'.BASE.'ordens/vincular?id='.$d->orden.'&p='.$d->id.'&c='.$d->codigo_produto.'">Vincular à outro produto cadastrado? </a>';
                }
                $tr .='</td>';
            }
            
            elseif ($f == 'revisado') {
                $tr .= '<td>' . retorna_revisado($d->$f) . '</td>';
            } elseif ($f == 'tipo') {
                $tr .= '<td>' . retorna_tipo($d->$f, $d->recorrente) . '</td>';
            } elseif ($f == 'aprovado') {
                $tr .= '<td>' . retorna_aprovado($d->$f) . '</td>';
            } elseif ($f == 'autorizado') {
                $tr .= '<td>' . retorna_autorizado($d->$f) . '</td>';
            } elseif ($f == 'analizado') {
                $tr .= '<td>' . retorna_analizado($d->$f) . '</td>';
            } elseif ($f == 'status') {
                $tr .= '<td>' . retorna_tarefa_status($d->$f) . '</td>';
            } elseif ($f == 'data') {
                $tr .= '<td>' . data_br($d->$f) . '</td>';
            } elseif ($f == 'data_vencimento') {
                $tr .= '<td>' . data_br($d->$f) . '</td>';
            } elseif ($f == 'valor') {
                $tr .= '<td> R$ ' . moeda_real($d->$f) . '</td>';
            } 
            elseif ($f == 'valor_total') {
                $tr .= '<td> R$ ' . moeda_real($d->$f) . '</td>';
            }
            elseif ($f == 'valor_pago') {
                $tr .= '<td>' . moeda_real($d->$f) . '</td>';
            } elseif ($f == 'data_pagamento') {
                $tr .= '<td>' . data_br($d->$f) . '</td>';
            } elseif ($f == 'data_final') {
                $tr .= '<td>' . data_br($d->$f) . '</td>';
            } else {
                $tr .= '<td>' . $d->$f . '</td>';
            }

        }
        if ($buttons) {
            $tr .= '<td>';
            if (!empty($buttons['detalhes'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['detalhes'] . "=" . $d->id . "' title='Visualizar'><i class='fa fa-search fa-lg'></i> </a>";
            }
            if (!empty($buttons['download'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' target='_blank' href='" . $d->link . "' title='Baixar'><i class='fa fa-download fa-lg'></i> </a>";
            }
            if (!empty($buttons['confirmar_pagamento'])) {
                if ($d->status == "aberto") {
                    $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['confirmar_pagamento'] . "=" . $d->id . "' title='Confirmar'><i class='fa fa-check fa-lg'></i> </a>";
                }
            }
            if (!empty($buttons['editar'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['editar'] . "=" . $d->id . "' title='Editar'><i class='fa fa-edit fa-lg'></i> </a>";
            }
            if (!empty($buttons['cancelar'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['cancelar'] . "=" . $d->id . "' title='Cancelar documento'><i class='fa fa-ban fa-lg'></i> </a>";
            }
            if (!empty($buttons['deletar'])) {
                $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['deletar'] . "=" . $d->id . "' title='Excluir'><i class='fa fa-trash fa-lg'></i> </a>";
            }
            if (!empty($buttons['concluir'])) {
                if ($d->status == "aberta") {
                    $tr .= "<a style='text-decoration: none; margin-right: 2px;' href='" . BASE . $buttons['concluir'] . "=" . $d->id . "' title='Concluir'><i class='fa fa-check fa-lg'></i> </a>";
                }
            }
            $tr .= '</td>';
        }
        $tr .= "</tr>";
    }

    echo '<div class="table-responsive"><table id="' . $id . '" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>' . $th . '</tr>
        </thead>
        <tbody>
        ' . $tr . '
        </tbody>
        </table></div>';
}

function modal_link($name, $modal, $class = "btn btn-primary")
{
    echo '<a style="margin-right: 5px;" data-toggle="modal" href="#' . $modal . '" class="' . $class . '">' . $name . '</a>';
}

function omodal($title, $id, $c = null)
{
    echo '<div id="' . $id . '" class="modal modal-styled">
        <div class="modal-dialog ' . $c . '">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title">' . $title . '</h3>
        </div> 
        <div class="modal-body">';
}


function cmodal()
{
    echo '
        </div> 
        </div>
        </div>
        </div>';
}

function dashboard_count($name, $value, $ico, $link, $color = null)
{   

    $color = ($color) ? 'style="color: '.$color.'"' : '';


    echo '
        <a href="'.BASE.$link.'">
        <div class="icon-stat">
        <div class="row">
        <div class="col-xs-12 text-left">
        <span class="icon-stat-label text-center">'  . $name . '</span> 
        <span class="icon-stat-value text-center" '.$color.' >' . $value . '</span> 
        </div>
        </div>
        </div>
        </a>
        ';
}

function dashboard_count_nota($name, $value, $ico, $id, $link, $color = null)
{   

    $color = ($color) ? 'style="color: '.$color.'"' : '';


    echo '
        <a href="'.BASE.$link.'">
        <div class="icon-stat">
        <div class="row">
        <div class="col-xs-12 text-left">
        <span class="icon-stat-label text-center">'  . $name . '</span> 
        <span id="'.$id.'" class="icon-stat-value text-center" '.$color.' >' . $value . '</span> 
        </div>
        </div>
        </div>
        </a>
        ';
}

function render_profile()
{
    $db = new db();
    $logo = $db->row('SELECT * FROM uploads WHERE modulo = "usuarios" AND modulo_key = ' . session('id') . ' ORDER BY id DESC');
    return $logo->link;
}

function render_logo()
{
    $db = new db();
    $empresa = $db->row('SELECT * FROM empresas WHERE id = 1');
    $logo = $db->row('SELECT * FROM uploads WHERE id = ' . $empresa->logo . ' ORDER BY id DESC');
    echo '<img width="150" src="' . $logo->link . '">';
}

function render_admin_logo()
{
    $db = new db();
    $empresa = $db->row('SELECT * FROM empresas WHERE id = 1');
    $logo = $db->row('SELECT * FROM uploads WHERE id = ' . $empresa->admin_logo . ' ORDER BY id DESC');
    echo '<img style="margin-top: 7px;" width="125" src="' . $logo->link . '">';
}

function render_favicon()
{
    $db = new db();
    $empresa = $db->row('SELECT * FROM empresas WHERE id = 1');
    $fv = $db->row('SELECT * FROM uploads WHERE id = ' . $empresa->favicon . ' ORDER BY id DESC');
    if($fv){
        echo '<link rel="shortcut icon" href="' . $fv->link . '">';
    }
}


function alert($type, $message)
{
    echo "<script>toastr." . $type . "('" . $message . "');</script>";
}

function line_chart($title)
{
    echo '<div class="portlet portlet-boxed">
		   <div class="portlet-header">
		        <h4 class="portlet-title"><u>' . $title . '</u></h4>
                   </div>
	           <div class="portlet-body">
        	      <div id="line-chart" class="chart-holder"></div>
            	   </div>
          </div>';
}

