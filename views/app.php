<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <title><?= (!empty($title)) ? $title : 'WEST' ?></title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="<?= PUBLIC_URL ?>css/font-awesome.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= PUBLIC_URL ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= PUBLIC_URL ?>css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?= PUBLIC_URL ?>css/select2.min.css">
    <link rel="stylesheet" href="<?= PUBLIC_URL ?>css/datepicker3.css">

    <!-- App CSS -->
    <link rel="stylesheet" href="<?= PUBLIC_URL ?>css/mvpready-admin.css">

    <!-- Favicon -->
    <?= render_favicon() ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?= PUBLIC_URL ?>js/jquery.js"></script>
    <script src="<?= PUBLIC_URL ?>js/bootstrap.min.js"></script>
    <script src="<?= PUBLIC_URL ?>js/jquery.slimscroll.min.js"></script>


    <script src="<?= PUBLIC_URL ?>js/jquery.dataTables.min.js"></script>
    <script src="<?= PUBLIC_URL ?>js/dataTables.bootstrap.js"></script>
    <script src="<?= PUBLIC_URL ?>js/select2.min.js"></script>

    <script src="<?= PUBLIC_URL ?>js/mvpready-core.js"></script>
    <script src="<?= PUBLIC_URL ?>js/mvpready-helpers.js"></script>
    <script src="<?= PUBLIC_URL ?>js/mvpready-admin.js"></script>


    <script src="<?= PUBLIC_URL ?>js/mvpready-account.js"></script>
    <script src="<?= PUBLIC_URL ?>js/form-validator/jquery.form-validator.min.js"></script>
    <script src="<?= PUBLIC_URL ?>js/bootstrap-datepicker.js"></script>
    <script src="<?= PUBLIC_URL ?>js/textboxio/textboxio.js"></script>
    <script src="<?= PUBLIC_URL ?>js/jquery.mask.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


</head>

<body class="account-bg">
    <header class="navbar" role="banner">
        <div class="container-fluid">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-cog"></i>
                </button>
                <a href="<?= BASE ?>usuarios/dashboard" class="navbar-brand navbar-brand-img">
                    <?= render_admin_logo() ?>
                </a>
            </div>
            <nav class="collapse navbar-collapse" role="navigation">
                <ul class="nav navbar-nav navbar-left">
                    <!--<li class="dropdown navbar-notification">
                  <a href="./page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell navbar-notification-icon"></i>
                    <span class="visible-xs-inline">&nbsp;Notoficações</span>
                    <b class="badge badge-primary">3</b>
                  </a>
                  <div class="dropdown-menu">
                    <div class="dropdown-header">&nbsp;Notificações</div>
                    <div class="notification-list">
                      <a href="./page-notifications.html" class="notification">
                    <span class="notification-icon"><i class="fa fa-cloud-upload text-primary"></i></span>
                    <span class="notification-title">Notification Title</span>
                    <span class="notification-description">Praesent dictum nisl non est sagittis luctus.</span>
                    <span class="notification-time">20 minutes ago</span>
                      </a>
                      <a href="./page-notifications.html" class="notification">
                    <span class="notification-icon"><i class="fa fa-ban text-secondary"></i></span>
                    <span class="notification-title">Notification Title</span>
                    <span class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</span>
                    <span class="notification-time">20 minutes ago</span>
                      </a>
                      <a href="./page-notifications.html" class="notification">
                    <span class="notification-icon"><i class="fa fa-warning text-tertiary"></i></span>
                    <span class="notification-title">Storage Space Almost Full!</span>
                    <span class="notification-description">Praesent dictum nisl non est sagittis luctus.</span>
                    <span class="notification-time">20 minutes ago</span>
                      </a>
                      <a href="./page-notifications.html" class="notification">
                    <span class="notification-icon"><i class="fa fa-ban text-danger"></i></span>
                    <span class="notification-title">Sync Error</span>
                    <span class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</span>
                    <span class="notification-time">20 minutes ago</span>
                      </a>
                    </div>
                    <a href="#" class="notification-link">Todas as notificações</a>
                  </div>
                </li>-->
                    <!--<li class="dropdown navbar-notification">
                  <a href="./page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope navbar-notification-icon"></i>
                    <span class="visible-xs-inline">&nbsp;Mensagens</span>
                  </a>
                  <div class="dropdown-menu">
                    <div class="dropdown-header">Messages</div>
                    <div class="notification-list">
                      <a href="./page-notifications.html" class="notification">
                    <div class="notification-icon"><img src="../../global/img/avatars/avatar-3-md.jpg" alt=""></div>
                    <div class="notification-title">New Message</div>
                    <div class="notification-description">Praesent dictum nisl non est sagittis luctus.</div>
                    <div class="notification-time">20 minutes ago</div>
                      </a>
                      <a href="./page-notifications.html" class="notification">
                    <div class="notification-icon"><img src="../../global/img/avatars/avatar-3-md.jpg" alt=""></div>
                    <div class="notification-title">New Message</div>
                    <div class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</div>
                    <div class="notification-time">20 minutes ago</div>
                      </a>
                      <a href="./page-notifications.html" class="notification">
                    <div class="notification-icon"><img src="../../global/img/avatars/avatar-4-md.jpg" alt=""></div>
                    <div class="notification-title">New Message</div>
                    <div class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</div>
                    <div class="notification-time">20 minutes ago</div>
                      </a>
                      <a href="./page-notifications.html" class="notification">
                    <div class="notification-icon"><img src="../../global/img/avatars/avatar-5-md.jpg" alt=""></div>
                    <div class="notification-title">New Message</div>
                    <div class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</div>
                    <div class="notification-time">20 minutes ago</div>
                      </a>
                    </div>
                    <a href="./page-notifications.html" class="notification-link">View All Messages</a>
                  </div>
                </li>
                <li class="dropdown navbar-notification empty">
                  <a href="./page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-warning navbar-notification-icon"></i>
                    <span class="visible-xs-inline">&nbsp;&nbsp;Alertas</span>
                  </a>
                  <div class="dropdown-menu">
                    <div class="dropdown-header">Alertas</div>
                    <div class="notification-list">
                      <h4 class="notification-empty-title">No alerts here.</h4>
                      <p class="notification-empty-text">Check out what other makers are doing on Explore!</p>
                    </div>
                    <a href="#" class="notification-link">Listar alertas</a>
                  </div>
                </li>
                <div class="dropdown-menu">
                    <div class="dropdown-header">&nbsp;Notificações</div>

                    <div class="notification-list">

                        <a href="./page-notifications.html" class="notification">
                            <span class="notification-icon"><i class="fa fa-cloud-upload text-primary"></i></span>
                            <span class="notification-title">Notification Title</span>
                            <span class="notification-description">Praesent dictum nisl non est sagittis luctus.</span>
                            <span class="notification-time">20 minutes ago</span>
                        </a>

                        <a href="./page-notifications.html" class="notification">
                            <span class="notification-icon"><i class="fa fa-ban text-secondary"></i></span>
                            <span class="notification-title">Notification Title</span>
                            <span class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</span>
                            <span class="notification-time">20 minutes ago</span>
                        </a>

                        <a href="./page-notifications.html" class="notification">
                            <span class="notification-icon"><i class="fa fa-warning text-tertiary"></i></span>
                            <span class="notification-title">Storage Space Almost Full!</span>
                            <span class="notification-description">Praesent dictum nisl non est sagittis luctus.</span>
                            <span class="notification-time">20 minutes ago</span>
                        </a>

                        <a href="./page-notifications.html" class="notification">
                            <span class="notification-icon"><i class="fa fa-ban text-danger"></i></span>
                            <span class="notification-title">Sync Error</span>
                            <span class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</span>
                            <span class="notification-time">20 minutes ago</span>
                        </a>

                    </div>

                    <a href="#" class="notification-link">Todas as notificações</a>

                </div>

                </li>
                <li class="dropdown navbar-notification">
                  <a href="./page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope navbar-notification-icon"></i>
                    <span class="visible-xs-inline">&nbsp;Mensagens</span>
                  </a>
                  <div class="dropdown-menu">
                    <div class="dropdown-header">Messages</div>
                    <div class="notification-list">
                      <a href="./page-notifications.html" class="notification">
                    <div class="notification-icon"><img src="../../global/img/avatars/avatar-3-md.jpg" alt=""></div>
                    <div class="notification-title">New Message</div>
                    <div class="notification-description">Praesent dictum nisl non est sagittis luctus.</div>
                    <div class="notification-time">20 minutes ago</div>
                      </a>
                      <a href="./page-notifications.html" class="notification">
                    <div class="notification-icon"><img src="../../global/img/avatars/avatar-3-md.jpg" alt=""></div>
                    <div class="notification-title">New Message</div>
                    <div class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</div>
                    <div class="notification-time">20 minutes ago</div>
                      </a>
                      <a href="./page-notifications.html" class="notification">
                    <div class="notification-icon"><img src="../../global/img/avatars/avatar-4-md.jpg" alt=""></div>
                    <div class="notification-title">New Message</div>
                    <div class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</div>
                    <div class="notification-time">20 minutes ago</div>
                      </a>
                      <a href="./page-notifications.html" class="notification">
                    <div class="notification-icon"><img src="../../global/img/avatars/avatar-5-md.jpg" alt=""></div>
                    <div class="notification-title">New Message</div>
                    <div class="notification-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit...</div>
                    <div class="notification-time">20 minutes ago</div>
                      </a>
                    </div>
                    <a href="./page-notifications.html" class="notification-link">View All Messages</a>
                  </div>
                </li>
                <li class="dropdown navbar-notification empty">
                  <a href="./page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-warning navbar-notification-icon"></i>
                    <span class="visible-xs-inline">&nbsp;&nbsp;Alertas</span>
                  </a>
                  <div class="dropdown-menu">
                    <div class="dropdown-header">Alertas</div>
                    <div class="notification-list">
                      <h4 class="notification-empty-title">No alerts here.</h4>
                      <p class="notification-empty-text">Check out what other makers are doing on Explore!</p>
                    </div>
                    <a href="#" class="notification-link">Listar alertas</a>
                  </div>
                </li>-->
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class=""><a href="<?= BASE ?>usuarios/dashboard"> Dashboard </a></li>
                    <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Documentos <i
                                class="fa fa-caret-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?= BASE ?>documentos">Documentos</a></li>
                        <li><a href="<?= BASE ?>documentos/analise">Para análise</a></li>
                        <?php if (adm()) { ?>
                            <li><a href="<?= BASE ?>documentos/pendentes">Pendentes no sistema</a></li>
                            <li><a href="<?= BASE ?>documentos/categorias">Categorias</a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Tasks <i
                                class="fa fa-caret-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?= BASE ?>tasks">Tasks</a></li>
                        <?php if (adm()) { ?>
                            <li><a href="<?= BASE ?>tasks/origens">Origens</a></li>
                            <li><a href="<?= BASE ?>tasks/tipos">Tipos</a></li>
                            <li><a href="<?= BASE ?>tasks/classificacoes">Classificação</a></li>
                        <?php } ?>
                    </ul>
                </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                            Compras
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?= BASE ?>crm/fornecedores">Fornecedores</a></li>
                            <li><a href="<?= BASE ?>ordens/compras">Compras</a></li>
                            <li> <a class="dropdown-item " href="<?= BASE ?>estoque/armazens"> Centros de Distribuições  </a>  </li>
                            <li> <a class="dropdown-item " href="<?= BASE ?>estoque/produtos"> Produtos  </a>  </li>
                            <li> <a class="dropdown-item " href="<?= BASE ?>estoque/produtos_categorias"> Categorias de produtos  </a>  </li>
                            <li> <a class="dropdown-item " href="<?= BASE ?>estoque/resumido"> Controle de estoque  </a>  </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                            Cursos
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="title-menu">CADASTROS</li>
                            <li><a href="<?= BASE ?>cursos/cursos">Cursos</a></li>
                            <li><a href="<?= BASE ?>turmas/turmas">Turmas</a></li>
                            <li><a href="<?= BASE ?>categorias/categorias">Categorias</a></li>
                            <li><a href="<?= BASE ?>planos/planos">Planos</a></li>
                            <li><a href="<?= BASE ?>certificados/certificados">Certificados / Carteiras</a></li>
                            <li><a href="<?= BASE ?>locais/locais">Locais</a></li>
                            <li><a href="<?= BASE ?>salas/salas">Salas</a></li>
                          
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                            Comercial
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?= BASE ?>crm/empresas">Clientes</a></li>
                            <li><a href="<?= BASE ?>#">Leads</a></li>
                            <li><a href="<?= BASE ?>ordens/vendas">Vendas</a></li>
                            <li><a href="<?= BASE ?>#">Ligações</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                            Financeiro
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="title-menu">MOVIMENTOS</li>
                            <li><a href="<?= BASE ?>financeiro/despesas">Despesas</a></li>
                            <li><a href="<?= BASE ?>financeiro/receitas">Receitas</a></li>
                            <li><a href="<?= BASE ?>financeiro/movimentos">Geral</a></li>
                            <li class="divider"></li>
                            <li class="title-menu">RELATÓRIO</li>
                            <li><a href="<?= BASE ?>financeiro/caixas">Extrato</a></li>
                            <li class="divider"></li>
                            <li class="title-menu">CADASTROS</li>
                            <li><a href="<?= BASE ?>financeiro/contas_bancarias">Contas</a></li>
                            <li><a href="<?= BASE ?>financeiro/contas_contabeis">Contas Contábeis</a></li>
                            <li><a href="<?= BASE ?>financeiro/categorias_financeiras">Categorias</a></li>
                            <li><a href="<?= BASE ?>financeiro/formas_pagamento">Pagamentos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                            Relatórios
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?= BASE ?>#">BI Cont4</a></li>
                        </ul>
                    </li>
                    <li class="dropdown ">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Treinamentos <i
                                  class="fa fa-caret-down"></i></a>
                      <ul class="dropdown-menu" role="menu">
                          <li><a href="<?= BASE ?>usuarios/treinamentos?id=<?= session('id') ?>">Meus Treinamentos</a>
                          </li>
                          <?php if (adm()) { ?>
                              <li><a href="<?= BASE ?>treinamentos">Treinamentos</a></li>
                              <li><a href="<?= BASE ?>treinamentos/escolas">Escolas</a></li>
                              <li><a href="<?= BASE ?>treinamentos/matriz">Matriz</a></li>
                          <?php } ?>
                      </ul>
                    </li>
                    <li class="dropdown navbar-profile">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                            <img src="<?= render_profile() ?>" class="navbar-profile-avatar" alt=""
                                onerror="this.onerror=null;this.src='<?= PUBLIC_URL ?>imagens/usuarios/1.png';">
                            <span class="visible-xs-inline"><?= session('nome') ?></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?= BASE ?>usuarios/profile">
                                    Meu Perfil
                                </a>
                            </li>
                            <li><a href="<?= BASE ?>usuarios">Usuários</a></li>
                            <li><a href="<?= BASE ?>empresas">Empresas</a></li>
                            <li><a href="<?= BASE ?>empresas/departamentos">Departamentos</a></li>
                            <li><a href="<?= BASE ?>empresas/funcoes">Cargos</a></li>
                            <li><a href="<?= BASE ?>empresas/informacoes">Informações</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= BASE ?>usuarios/logout ">
                                   
                                    &nbsp;&nbsp;Sair
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <div class="content">
        <div class="container-fluid">
            <?php if (has_flash('error')) { ?>
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                <strong>Ops!</strong> <?= flash_message('error') ?>
            </div>
            <?php } ?>

            <?php if (has_flash('success')) { ?>
            <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                <strong>Show!</strong> <?= flash_message('success') ?>
            </div>
            <?php } ?>

            <?= $_view ?>
        </div>
    </div>
    <div class="text-center">Cont4 © <?= date("Y") ?> Direitos Reservados </div>
    <script>
    $.validate({
        modules: 'security, brazil',
        lang: 'pt'
    });
    </script>


    <!--<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/56b4e4224003e62e1739f2a2/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
})();
</script>-->
    <!--End of Tawk.to Script-->

</body>

</html>