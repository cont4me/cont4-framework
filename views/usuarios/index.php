<?php
	modal_link('+ Adicionar', 'add');
	br();
	ptable('Usuários');
	datatable('categorias', ['Nome', 'E-mail', 'Regra'], ['nome', 'email', 'regra'], $usuarios,
		[
			'editar' => 'usuarios/editar?id',
			'treinamentos' => 'usuarios/treinamentos?id',
			'senha' => 'usuarios/senha?id',
			'cancelar' => 'usuarios/cancelar?id',
		]);
	cpanel();

	omodal('Novo usuário', 'add');
		form_open('usuarios/index');

			hidden('empresa', session('empresa'));
			$regra = [['value' => 'admin', 'nome' => 'Admin'],  ['value' => 'vendedor', 'nome' => 'Vendedor'], ['value' => 'usuario', 'nome' => 'Usuário']];
			$status =[['value' => 'ativo', 'nome' => 'Ativo'],['value' => 'inativo', 'nome' => 'Inativo']];
			$instrutor = [['value' => '0', 'nome' => 'Não'],['value' => '1', 'nome' => 'Sim']];

			form_text_input('Nome', 'nome', 'required');
			form_text_input('E-mail', 'email', 'server', 'usuarios/valida_email');
			form_password_input('Senha', 'senha', 'required');
			form_text_input('Telefone', 'telefone');
			form_select2_data('Regra', 'regra', $regra, 'nome');
			form_select2_data('Status', 'status', $status, 'nome');
			form_select2_data('Instrutor', 'instrutor', $instrutor, 'nome');
			row();
				col(6);
					form_int_input('Percentual sobre venda', 'percentual_venda');
				cdiv();
				col(6);
					form_int_input('Percentual de desconto', 'limite_desconto');
				cdiv();
			cdiv();

			submit('Salvar', 'btn btn-primary');
		form_close();
	cmodal();

?>
