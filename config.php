<?php



    define('BASE', 'http://localhost/west/');
    define('HTDOCS', '/west');
    define('PUBLIC_URL', BASE . '/public/');
    define('PUBLIC_UPLOAD', BASE . '/uploads/');
    define('PAGE_LOGOUT', 'usuarios/login');


    define('DEFAULT_CLASS', 'usuarios');
    define('DEFAULT_METHOD', 'index');

    define('DB_HOST','localhost');
    define('DB_USER','root');
    define('DB_PASS','');
    define('DB_NAME','west');

