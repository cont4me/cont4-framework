<?php

function limite_usuarios($id)
{
	$database = new db();
	$empresa = $database->row('SELECT * FROM empresas WHERE id=' . $id);
	return $empresa->limite_usuarios;
}

function total_usuarios($id)
{
	$database = new db();
	$count = $database->table('SELECT count(id) FROM usuarios WHERE empresa=' . $id);
	return $count;
}

function tamanho_usado($id)
{
	$database = new db();
	$count = $database->table('SELECT sum(size) as total FROM uploads WHERE empresa=' . $id);

	foreach ($count as $v) {
		 return  $v->total / 1000;
	}
}

function tamanho_geral($id)
{
	$database = new db();
	$count = $database->row('SELECT * FROM empresas WHERE id=' . $id);
	return $count->limite_espaco;
}

function pode_aprovar($id, $usuario)
{
	$database = new db();
	$count = $database->table('SELECT * FROM documentos_aprovacoes WHERE documento = ' . $id .' AND revisado = 0');
	if(!empty($count)) {
		return false;
	}

	return true;
}

function pode_revisar($id, $usuario)
{
	$database = new db();
	$count = $database->table('SELECT * FROM documentos_aprovacoes WHERE documento = ' . $id .' AND usuario = ' . $usuario . ' AND revisado = 1');
	if(!empty($count)) {
		return false;
	}
	return true;
}


function responsavel($task, $usuario)
{
	$database = new db();
	$count = $database->table('SELECT * FROM tasks_analizadas WHERE task = ' . $task .' AND usuario = ' . $usuario);
	if(!empty($count)) {
		return true;
	}
	return false;
}

