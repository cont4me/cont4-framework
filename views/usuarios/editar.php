<?php
	opanel('Editar usuario: ' . $usuario->nome);
		form_open('usuarios/editar?id='.get('id'));

			$regra = [['value' => 'admin', 'nome' => 'Admin'], ['value' => 'vendedor', 'nome' => 'Vendedor'], ['value' => 'usuario', 'nome' => 'Usuário']];
			$status =[['value' => 'ativo', 'nome' => 'Ativo'],['value' => 'inativo', 'nome' => 'Inativo']];
			$vendedor = [['value' => '0', 'nome' => 'Não'],['value' => '1', 'nome' => 'Sim']];
			form_text_input('Nome', 'nome', 'required','','', $usuario->nome);
			form_text_input('Telefone', 'telefone','','','', $usuario->telefone);
			form_select2_data('Regra', 'regra', $regra, $usuario->regra);
			form_select2_data('Status', 'status', $status, $usuario->status);
			row();
				col(6);
					form_int_input('Percentual sobre venda', 'percentual_venda','','','', $usuario->percentual_venda);
				cdiv();
				col(6);
					form_int_input('Percentual de desconto', 'limite_desconto', '','','', $usuario->limite_desconto);
				cdiv();
			cdiv();
			submit('Salvar', 'btn btn-primary');

		form_close();

	cpanel();


?>
